## EB setup steps:

1. Setup git: init, add, commit etc. EB CLI will only consider committed changes

2. Build and push image to ECR (if you do this only dockerrun.aws.json file needed, else you need to .zip both this file and dockerfile together)

3. . Create dockerrun.aws.json: file that tells EB how to run your container, similiar to ECS job definition. This specifies ECR repo and tag. 

4. **eb init** – follow prompts: creates application 
5. **eb create** – follow prompts: creates environment
6. **eb status –verbose** once created
7. **eb terminate** to delete if needed (might need to do eb use to specify environment)


## EB deploy changes:

1. **git commit** 

2. **docker build && docker push**

2. **eb deploy**: pushes latest commit 

The bitbucket pipelines build container will need to do the same commands as above 

## Monitoring:

1. **eb ssh** with .ssh key set up will give access to primary EC2 that has been provisioned. EC2 has docker client on it so you can bash into containers. 

## Resources:
https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker.container.console.html

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/concepts-webserver.html


## Notes:

Replace generic references with account specific details in ecsPush.sh, dockerrun etc. 

Ensure provisioned EC2 instances has appropriate IAM permissions to retrieve image from ECR <- can allocate EcrReadAccess policy to the EC2:
https://stackoverflow.com/questions/39083768/aws-docker-deployment




