#kill running containers
docker kill $(docker ps -q)
#remove stopped containers
docker container prune -f
#build image
docker build -t [ACCOUNT_ID].dkr.ecr.ap-southeast-2.amazonaws.com/[REPO NAME]:[TAG] .
#get ECR login command and run
$(aws ecr get-login  --no-include-email --region ap-southeast-2)
#push to ECR repo
docker push [ACCOUNT_ID].dkr.ecr.ap-southeast-2.amazonaws.com/[REPO NAME]:[TAG]