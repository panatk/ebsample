
#stop running containers
docker kill $(docker ps -q)
#remove stopped containers
docker container prune -f
#rebuild from dockerfile
docker build -t test:latest .
#run as daemon (creating a container from image)
docker run -td test:latest
#remove images without atleast one container associated to them
docker image prune -f

#exec call for the scripts - put your initial command here
#docker exec -it $(docker ps -q) bash # python3 /containerScripts/containerMain.py test
